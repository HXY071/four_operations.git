package test;

import java.util.Random;

public class Arithmetic {
	int a = new Random().nextInt(6);    //整数计算的条件判断，0为加，1为减，2为乘，3为除（除法中也有判断被除数是否为0），4为三个数的乘法计算，5为三个数的除法计算
	int b = new Random().nextInt(10)%(10-1+1) + 1;//分数计算参数
	int c = new Random().nextInt(10)%(10-2+1) + 2;//分数计算参数
	int d = new Random().nextInt(10)%(10-1+1) + 1;//分数计算参数
	int e = new Random().nextInt(10)%(10-2+1) + 2;//分数计算参数
	int k = new Random().nextInt(10)%(10-1+1) + 1;//分数计算参数
	int l = new Random().nextInt(10)%(10-2+1) + 2;//分数计算参数
	int f = new Random().nextInt(100);    //整数计算参数
	int g = new Random().nextInt(100);    //整数计算参数
	int h = new Random().nextInt(100); 
	int j = new Random().nextInt(2); //三个数的计算里的加减选择
	String astr="";
	String qstr="";
	boolean x;
	public Arithmetic(boolean x) {
		this.x=x;
	}
	public String int_operation()
	{
		int result = 0;
		if(a==0)
			result=f+g;
		if(a==1)
			result=f-g;
		if(a==2)
			result=f*g;
		astr = String.valueOf( result);
		if(a==3)
		{
			if(g==0)
			{
				astr=int_operation();
				return astr;
			}
			else
			{
				if(g!=0&&g!=1){
					int d=common_divisor(f,g);  //公约数计算
					f=f/d;
					g=g/d;
					astr = (f+"/"+g);  //直接显示分数，不显示为小数点形式
				}
				if(g==1)
					astr=(""+f);
			}
			
		}
		if(a==4)
		{
			if(j==0)
				result=(f+g)*h;
			if(j==1)
				result=(f-g)*h;
		}
		if(a==5)
		{
			if(j==0)
			{
				int o=f+g;
				if(h==0)
				{
					astr=int_operation();
					return astr;
				}
				else
				{
					if(h!=0&&h!=1){
						int d=common_divisor(o,h);  //公约数计算
						o=o/d;
						h=h/d;
						astr = (o+"/"+h);  //直接显示分数，不显示为小数点形式
					}
					if(h==1)
						astr=(""+o);
				}
			}
			if(j==1)
			{
				int o=f-g;
				if(h==0)
				{
					astr=int_operation();
					return astr;
				}
				else
				{
					if(h!=0&&h!=1){
						int d=common_divisor(o,h);  //公约数计算
						o=o/d;
						h=h/d;
						astr = (o+"/"+h);  //直接显示分数，不显示为小数点形式
					}
					if(h==1)
						astr=(""+o);
				}
			}			
	    }
		return astr;
	}
		
	public String fra_operation(){
		this.b = new Random().nextInt(10)%(10-1+1) + 1;
		this.c = new Random().nextInt(10)%(10-2+1) + 2;
		this.d = new Random().nextInt(10)%(10-1+1) + 1;
		this.e = new Random().nextInt(10)%(10-2+1) + 2;
		this.k = new Random().nextInt(10)%(10-1+1) + 1;
		this.l = new Random().nextInt(10)%(10-2+1) + 2;
		if(c<b||e<d||l<k||c%b==0||e%d==0||l%k==0)
		{
			astr=fra_operation();
			return astr;
		}
			
		int fz=1,fm=c*e,fz1=1,fm1=1;  //分子，分母
		if(a==0)
			fz=b*e+c*d;
		if(a==1){
			fz=b*e-c*d;
			if(fz==0)
			{
				return astr=("0");
			}
		}		
		if(a==2)
			fz=b*d;
		if(a==3)
		{
			fz=b*e;
			fm=c*d;
		}
		if(a==4)
		{
			if(j==0)
			{
				fz=b*e+c*d;
				fz1=fz*k;
				fm1=fm*l;
			}
			if(j==1)
			{
				fz=b*e-c*d;				
				if(fz==0)
					return astr=("0");	
				else
				{
					fz1=fz*k;
					fm1=fm*l;
				}				
			}
		}
		if(a==5)   //5为三个数的除法计算
		{
			if(j==0) //+
			{
				fz=b*e+c*d;	
				fz1=fz*l;
				fm1=fm*k;
			}
			if(j==1)   //-
			{
				fz=b*e-c*d;
				if(fz==0)
					return astr=("0");
				else
				{
					fz1=fz*l;
					fm1=fm*k;				
				}
			}
		}
		int f=common_divisor(fm1,fz1);
		if(f>0){
			fm1=fm1/f;
			fz1=fz1/f;
		}
		if(f<0){
			fm1=-fm1/f;
			fz1=-fz1/f;
		}
		astr = (fz1+"/"+fm1);
		return astr;
		
	}
	public static int common_divisor(int m,int n)
	{
		while(m%n!=0){
			int t=m%n;
			m=n;
			n=t;
		}
		return n;
	}
	public String toString(){
		if(x==true){
			if(a==0)
				qstr=(f+"+"+g+"=");
			if(a==1)
				qstr=(f+"-"+g+"=");
			if(a==2)
				qstr=(f+"×"+g+"=");
			if(a==3)
				qstr=(f+"÷"+g+"=");
			if(a==4)
			{
				if(j==0)
				{
					qstr=("("+f+"+"+g+")*"+h+"=");
				}
				if(j==1)
				{
					qstr=("("+f+"-"+g+")*"+h+"=");
				}
			}
			if(a==5)
			{
				if(j==0)
				{
					qstr=("("+f+"+"+g+")÷"+h+"=");
				}
				if(j==1)
				{
					qstr=("("+f+"-"+g+")÷"+h+"=");
				}
			}	
		}
		if(x==false){
			if(a==0)
				qstr=(b+"/"+c+"+"+d+"/"+e+"=");
			if(a==1)
				qstr=(b+"/"+c+"-"+d+"/"+e+"=");
			if(a==2)
				qstr=(b+"/"+c+"×"+d+"/"+e+"=");
			if(a==3)
				qstr=(b+"/"+c+"÷"+d+"/"+e+"=");
			if(a==4)
			{
				if( j==0)
				{
					qstr=("("+b+"/"+c+"+"+d+"/"+e+")*"+k+"/"+l+"=");
				}
				if(j==1)
				{
					qstr=("("+b+"/"+c+"-"+d+"/"+e+")*"+k+"/"+l+"=");
				}
			}
			if(a==5)
			{
				if(j==0)
				{
					qstr=("("+b+"/"+c+"+"+d+"/"+e+")÷"+k+"/"+l+"=");
				}
				if(j==1)
				{
					qstr=("("+b+"/"+c+"-"+d+"/"+e+")÷"+k+"/"+l+"=");
				}
			}
		}
		return qstr;
	}
	
}
